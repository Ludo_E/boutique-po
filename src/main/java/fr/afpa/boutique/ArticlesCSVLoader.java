package fr.afpa.boutique;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.PatternSyntaxException;

public class ArticlesCSVLoader {

    /**
     * Constante définissant le séparateur utilisé dans le CSV traité.
     */
    public final static String SEPARATOR = ";";

    /**
     * Charge les Articles contenus dans un CSV correctement formaté.
     * 
     * param nomFichier Le nom du fichier CSV à charger
     */
    public static ArrayList<Article> chargerArticles(String nomFichier) {
        // récupération du chemin complet du fichier concerné
        final File f = new File("");
        final String cheminFichier = f.getAbsolutePath() + File.separator + nomFichier;

        File fichierALire = new File(cheminFichier);

        // récupération de toutes les lignes du fichier CSV
        ArrayList<String> lignesFichier = lireLignesFichiers(fichierALire);
        ArrayList<Article> articles = new ArrayList<Article>();
        try {
            for (String ligne : lignesFichier) {
                String[] informationsArticle = ligne.split(SEPARATOR);

                // récupération des informations séparement
                String nom = informationsArticle[0];
                double prixHorsTaxe = Double.parseDouble(informationsArticle[1]);
                double tva = Double.parseDouble(informationsArticle[2]);
                String description = informationsArticle[3];

                Article article = new Article(nom, prixHorsTaxe, tva, description);

                articles.add(article);
            }
        } catch (PatternSyntaxException e) {
            System.err.println("Erreur lors de la lecture du fichier CSV.");
        }

        return articles;
    }

    public static ArrayList<String> lireLignesFichiers(File file) {
        ArrayList<String> result = new ArrayList<String>();

        FileReader fileReader;
        try {
            fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            // lecture du fichier ligne par ligne
            for (String line = bufferedReader.readLine(); line != null; line = bufferedReader.readLine()) {
                result.add(line);
            }

            // Fermeture des streams pour libération des ressources
            bufferedReader.close();
            fileReader.close();

        } catch (IOException e) {
            System.err.println(e.getMessage());
        }

        return result;
    }
}
