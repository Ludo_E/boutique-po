package fr.afpa.boutique;

public class Article {

//// Début des attributs

    /**
     * Dénomination textuelle de l'article
     */
    private String name;

    private double prixHorsTaxe;

    /**
     * TVA exprimée en pourcentage
     * Exemples : 10; 5.5
     */
    private double tva;

    private String description;
/// Fin de la déclaration des attributs

    public Article(String name, double prixHorsTaxe, double tva, String description) {
        this.name = name;
        this.prixHorsTaxe = prixHorsTaxe;
        this.tva = tva;
        this.description = description;
    }

    // TODO créer les setters et les getters

    // TODO implémenter toString pour un affichage correct en console

} // fin de la classe Article
