package fr.afpa.boutique;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class Client {

    private int numeroClient;
    private LocalDate dateDernierAchat;
    private String numeroTelephone;
    private String email;
    private ArrayList<Article> articlesAchetes;

    // TODO générer les getters et les setters

    public Client(int numeroClient, String numeroTelephone, String email) {
        this.numeroClient = numeroClient;
        this.dateDernierAchat = dateDernierAchat;
        this.numeroTelephone = numeroTelephone;
        this.email = email;
    }

    /**
     * Indique si le client est considéré comme fidèle.
     * 
     * Règle de fidelité : le client est considéré comme fidèle à partir de 4 achats dans la boutique
     */
    public boolean clientFidele() {
        boolean estFidele = false;
        // TODO tester si le client est fidèle
        return estFidele;
    }

    /**
     * Calcule la somme des dépenses du client
     * @return
     */
    public double totalDepense() {
        // TODO faire la somme des dépenses pour tous les articles achetés
        double totalDepense = 0.0;
        // Comment faire pour additionner toutes les dépenses ?
        // Quels sont les attributs à utiliser ?
        return totalDepense; 
    }

    /**
     * Ajoute un achat à la liste des achats du client
     * @param article L'article acheté
     */
    public void ajouterAchat(Article article) {
        // TODO ajouter l'achat

        // TODO attention de bien mettre la date du dernier achat à jour avec la date actuelle

        // Pour obtenir la date actuelle vous pouvez utiliser le code suivant
        // LocalDate dateActuelle = LocalDate.now();
    }
}
