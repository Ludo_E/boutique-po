# Application de gestion d'une boutique

Exercice d'entraînement à la programmation orientée objet en Java.

Des "TODO" indiquent les endroits où le code est à compléter.

Vous trouverez ci-dessous le diagramme de classe du projet (demandez à votre formateur.trice pour une explication).

![Diagramme de classe du projet](./boutique.svg)

Bon courage !